import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { StartPage } from '../start/start';
import { GlobalProvider } from '../../providers/global/global';

/**
 * Generated class for the DesertionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-termConditions',
  templateUrl: 'termConditions.html',
})
export class TermConditionsPage {
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public menuCtrl: MenuController,
              public globalProv: GlobalProvider) {

    this.menuCtrl.enable(false,'MenuObras');
  }

   goNextStep(){
    //marcar que acepto los terminos y condiciones
    this.globalProv.setItemLocalStorage(GlobalProvider.HAS_USER_AGREED_KEY,{value:true})
    this.navCtrl.setRoot(StartPage, {}, {animate: true, direction: 'forward'});
   }

   goBack(){
    navigator['app'].exitApp();
   }

}