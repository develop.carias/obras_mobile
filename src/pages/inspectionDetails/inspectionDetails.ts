import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams,AlertController, Nav, App, Platform, ModalController } from 'ionic-angular';
import { InspectionInformPage } from '../inspectionInform/inspectionInform';
import { GlobalProvider } from '../../providers/global/global';
import { CommentPage } from '../comment/comment';
import { Observable } from 'rxjs/Observable';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { FilePath } from '@ionic-native/file-path';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-inspection-details',
  templateUrl: 'inspectionDetails.html'
})
export class InspectionDetailsPage {

  private win: any = window;    

  @ViewChild(Nav) nav: Nav;
  observations : any[] = [];
  inspection: any = {}
  inspectionInProgress: any = {};
  peoplesFix : any [] = [];

  //Variables de la vista
  evaluationOptions:any=[];
  selectedObservationsList: any=[];
  optionsSelectedList: any=[];
  levelSelected:any;
  isObservations: any
  isEvaluation:any
  searchObservationsEvaluations: string;
  currentSelectedObservationList : any = [];
  dont_allow_continue : boolean = false;
  keyboardWillShow$: Observable<any>;
  keyboardWillShow : boolean = false;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public globalProvider : GlobalProvider,
              public alertController: AlertController,
              public app : App,
              public platform : Platform,
              public modalCtrl : ModalController,
              public camera : Camera,
              public filePath : FilePath,
              public file : File,
              public transfer : FileTransfer) {
    this.inspection=navParams.get('inspection');
    this.ChangeBackButton();
    this.inspectionInProgress = this.globalProvider.getItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY);

    if(this.globalProvider.getItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY).inspectionInform.observations == null){
      this.inspection.protocol.observations.forEach((element,index,[]) => {
        element.itemClass = "observaciones";
        element.obsTitleClass = "observationbluebox";
        element.arrowClass = "button-obs-details";
        element.selected = false;
        element.errorEval = false;
        element.evaluations.forEach(evaluation => {
          evaluation.media = [];
          evaluation.selected = false;
          evaluation.urlImage = null;
          evaluation.urlVideo = null;
          evaluation.comment = null;
          evaluation.latitude = null;
          evaluation.longitude = null;
          evaluation.time = null;
          evaluation.imgCounter = 0;
          evaluation.vidCounter = 0;
          if(!evaluation.options){
            evaluation.optionSelected_id = 1;
          }else{
            evaluation.optionSelected_id = 0;
          }
          evaluation.defaultSelectedOption_id = 0;
          evaluation.options.forEach(option => {
            if(option.pivot.defaultOption == 1){
              evaluation.defaultSelectedOption_id = option.id
            }
          });
        });
        this.observations.push(element);
      });

      this.inspectionInProgress.inspectionInform.observations = this.observations;
      this.inspectionInProgress.dont_allow_continue = false;
      this.inspectionInProgress.selectedOptionDirectly = false;
      this.inspectionInProgress.takedMedia = [];
    }else{
      this.observations = this.inspectionInProgress.inspectionInform.observations;
    }

    this.isObservations = true;
    this.isEvaluation = false;
  }
 
  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      this.goBack();
  });
  }

  copyFileToDirectory(correctPath : string, currentName : string, finalPath : string, finalName : string, itemObs : any){
    this.file.copyFile(correctPath, currentName, finalPath, finalName).then(success => {
      /* itemObs.imageFilePath = finalPath + finalName;
      itemObs.urlImage = this.win.Ionic.WebView.convertFileSrc(itemObs.imageFilePath) + '?' + new Date().getTime();
      itemObs.imageFileName = finalName; */
      itemObs.urlImage = '1';
      let media = {
        image:{
          mediaFilePath: finalPath + finalName,
          urlMedia: this.win.Ionic.WebView.convertFileSrc(finalPath + finalName) + '?' + new Date().getTime(),
          mediaFileName: finalName
        },
        video:{
          mediaFilePath: null,
          urlMedia: null,
          mediaFileName: null
        }
      };
      itemObs.media.push(media);
      itemObs.imgCounter++;
      this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
    }, error => {
      this.globalProvider.messagesToast("No se pudo guardar la foto en directorio temporal");
    });
  }

  checkFileAndCopy(correctPath : string, currentName : string, finalPath : string, finalName : string, itemObs : any){
    this.file.checkFile(finalPath, finalName).then(existFile => {
      if(!existFile){
        this.copyFileToDirectory(correctPath, currentName, finalPath, finalName, itemObs);
      }else{
        this.file.removeFile(finalPath, finalName).then(result => {
          this.copyFileToDirectory(correctPath, currentName, finalPath, finalName, itemObs);
        }).catch(err => {

        });
      }
    }).catch(err => {

      if(err.code == 1){
        this.copyFileToDirectory(correctPath, currentName, finalPath, finalName, itemObs);
      }
    });
  }

  addPhoto(itemObs : any){
    if(itemObs.imgCounter >= 3){
      this.globalProvider.messagesToast("No puede tener más de 3 imágenes por evaluación");
      return;
    }
    /* let finalPath = '/';
    let finalName = 'Foto_' + itemObs.media.length + '_eval_' + itemObs.id + '_de_plan_' + this.inspection.id + '.jpg';
    itemObs.urlImage = '1';
    let media = {
      image:{
        mediaFilePath: finalPath + finalName,
        urlMedia: finalName,
        mediaFileName: finalName
      },
      video:{
        mediaFilePath: null,
        urlMedia: null,
        mediaFileName: null
      }
    };
    itemObs.media.push(media);
    console.log("Media object", media);
    console.log("Media array", itemObs.media);
    itemObs.imgCounter++;
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress); */

    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      targetWidth: 500,
      targetHeight: 500,
      //allowEdit: true,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG
    };
    
    this.camera.getPicture(options).then(imagePath => {
      let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
      let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
      let newDirectory = 'media_inform_plan_' + this.inspection.id;
      let finalPath = GlobalProvider.APP_DIRECTORY_PATH_INTERN_MEMORY + newDirectory + '/';
      let finalName = 'Foto_' + itemObs.media.length + '_eval_' + itemObs.id + '_de_plan_' + this.inspection.id + '.jpg';
      this.file.checkDir(GlobalProvider.APP_DIRECTORY_PATH_INTERN_MEMORY, newDirectory).then(existDirectory => {

        if(!existDirectory){
          this.file.createDir(GlobalProvider.APP_DIRECTORY_PATH_INTERN_MEMORY, newDirectory, true).then((result) => {

            this.checkFileAndCopy(correctPath, currentName, finalPath, finalName, itemObs);
          }).catch(err => {

          });    
        }else{
          this.checkFileAndCopy(correctPath, currentName, finalPath, finalName, itemObs);
        }
      }).catch(err => {

        if(err.code == 1){
          this.file.createDir(GlobalProvider.APP_DIRECTORY_PATH_INTERN_MEMORY, newDirectory, true).then((result) => {

            this.checkFileAndCopy(correctPath, currentName, finalPath, finalName, itemObs);
          }).catch(err => {

          });
        }
      });
    }, error => {
      console.log(error,"Este es el error de camera.");
    });
  }

  async deleteMedia(mediaToDelete : any, itemObs : any, image : boolean){
    await this.file.removeFile(mediaToDelete.mediaFilePath.substr(0, mediaToDelete.mediaFilePath.lastIndexOf('/') + 1), mediaToDelete.mediaFileName).then(result => {
      this.globalProvider.messagesToast("Se borró el archivo exitosamente", undefined, undefined, "toast-success");
    }).catch(err => {
      this.globalProvider.messagesToast("No se pudo encontrar el archivo")
    });
    mediaToDelete.mediaFilePath = null;
    mediaToDelete.urlMedia = null;
    mediaToDelete.mediaFileName = null;
    image? itemObs.imgCounter-- : itemObs.vidCounter-- ;
    if(itemObs.urlImage && image){
      itemObs.urlImage = this.checkIfMediaExists(itemObs.media, image)? itemObs.urlImage : null;
    }
    if(itemObs.urlVideo && !image){
      itemObs.urlVideo = this.checkIfMediaExists(itemObs.media, image)? itemObs.urlVideo : null;
    }
  }

  checkIfMediaExists(mediaArray : any[], image : boolean) : boolean{
    let bool : boolean;
    if(image){
      bool = mediaArray.some(media=>{
        return (media.image.urlMedia);
      });
    }else{
      bool = mediaArray.some(media=>{
        return (media.video.urlMedia);
      });
    }
    return bool;
  }

  filterObservations(ev : any){
    this.inspectionInProgress.inspectionInform.observations = this.observations;

    const desiredObservation = ev.target.value;

    if (desiredObservation && desiredObservation.trim() != '') {
      this.inspectionInProgress.inspectionInform.observations = this.inspectionInProgress.inspectionInform.observations.filter((obs) => {
        return (obs.description.toLowerCase().indexOf(desiredObservation.toLowerCase()) > -1 || obs.short_description.toLowerCase().indexOf(desiredObservation.toLowerCase()) > -1);
      })

      if(this.isEvaluation){
        this.selectedObservationsList = this.currentSelectedObservationList;
        this.selectedObservationsList = this.selectedObservationsList.filter(evaluation => {
          return (evaluation.description.toLowerCase().indexOf(desiredObservation.toLowerCase()) > -1 || evaluation.short_description.toLowerCase().indexOf(desiredObservation.toLowerCase()) > -1);
        })
      }
    }
  }

  restoreEvaluations(){
    this.selectedObservationsList = this.currentSelectedObservationList;
  }

  addComments(itemObs) {
    let comment = this.modalCtrl.create(CommentPage, {existentcomment: itemObs.comment}, {cssClass: 'comment'});
    comment.present();
    comment.onDidDismiss(comentario => {
      itemObs.comment = comentario;
    });
  }

  goDetails(selectedItem){
    this.searchObservationsEvaluations = '';

    this.selectedObservationsList = []

    selectedItem.evaluations.forEach(element => {
      this.selectedObservationsList.push(element);
    });

    this.currentSelectedObservationList = this.selectedObservationsList;

    this.isObservations=false;
    this.isEvaluation=true;

    this.levelSelected=selectedItem;
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  selectEvaluation(pressedCheckbox,ev : any){
    if(ev.srcElement != null){
      if(ev.srcElement.localName == 'h2'){
        pressedCheckbox.selected = !pressedCheckbox.selected;
      }
    }
    if(pressedCheckbox.showOptions != pressedCheckbox.selected){
      pressedCheckbox.showOptions = true;
    }
    if(pressedCheckbox.selected == false){
      pressedCheckbox.optionSelected_id = 0;
    }else{
      if(this.inspectionInProgress.selectedOptionDirectly){
        this.inspectionInProgress.selectedOptionDirectly = false;
      }else{
        pressedCheckbox.optionSelected_id = pressedCheckbox.defaultSelectedOption_id;
      }
    }
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  goOptionsDetail(selectedItem){
    selectedItem.showOptions=!selectedItem.showOptions;

    this.optionsSelectedList = []

    selectedItem.options.forEach(element => {
      this.optionsSelectedList.push(element);
    });
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  updateonoptions(option : any, itemObs : any){
    if(!itemObs.selected){
      this.inspectionInProgress.selectedOptionDirectly = true;
      itemObs.selected = true;
    }
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

  checkEvaluationsToContinue(obs : any[]) : boolean{
    let bool = false;
    let evaluations : any [] = [];

    obs.forEach(obs => {
      obs.evaluations.forEach(evaluation => {
        evaluations.push(evaluation);
      });
    });

    bool = evaluations.some(evaluation => {
      return (evaluation.selected && evaluation.optionSelected_id == 0)
    });

    return bool
  }

  goNextStep(){
    this.inspectionInProgress.dont_allow_continue = this.checkEvaluationsToContinue(this.inspectionInProgress.inspectionInform.observations);
    if(this.inspectionInProgress.dont_allow_continue){
      this.globalProvider.messagesToast("Debe elegir una opción para cada evaluación seleccionada")
    }else{
      this.inspectionInProgress.inspectionInform.observations = this.observations;
      this.selectedObservationsList = this.currentSelectedObservationList;
      this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
      this.searchObservationsEvaluations = '';
      let aux_inform = JSON.parse(JSON.stringify(this.inspectionInProgress.inspectionInform));
      this.navCtrl.push(InspectionInformPage, {inspection : this.inspection, 
                                              aux_inform:aux_inform}, {animate: true, direction: 'forward'});
    }
  }

  showObservationWithError(observations : any[]){
    let observationHolder : any = {};
    let IsError : boolean = false;
    let correct : boolean = false;
    observationHolder = observations.find(obs => {
      return (obs.id == this.levelSelected.id)
    });
    IsError = this.levelSelected.evaluations.some(evaluation => {
      if(evaluation.selected && evaluation.optionSelected_id != 0 && !correct){
        correct = true;
      }
      return (evaluation.selected && evaluation.optionSelected_id == 0)
    });
    if(observationHolder){
      if(IsError){
        observationHolder.errorEval = true;
        observationHolder.itemClass = "observaciones";
        observationHolder.obsTitleClass = "observationbluebox";
        observationHolder.arrowClass = "button-obs-details";
      }else{
        observationHolder.errorEval = false;
        if(correct){
          observationHolder.itemClass = "correctObs";
          observationHolder.obsTitleClass = "correctTitleObs";
          observationHolder.arrowClass = "correctArrowObs";
        }else{
          observationHolder.itemClass = "observaciones";
          observationHolder.obsTitleClass = "observationbluebox";
          observationHolder.arrowClass = "button-obs-details";
        }
      }
    }
  }

  goBack(){
    this.searchObservationsEvaluations = '';
    this.inspectionInProgress.inspectionInform.observations = this.observations;
    this.selectedObservationsList = this.currentSelectedObservationList;
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
    if (this.isEvaluation){
      this.levelSelected.evaluations.forEach(evaluation => {
        evaluation.showOptions = false;
      });
      this.showObservationWithError(this.inspectionInProgress.inspectionInform.observations);
      this.isEvaluation = false;
      this.isObservations = true;
      this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
    }else if(this.isObservations){
      this.navCtrl.pop({animate: true, direction: 'backward'});    
    }
  }

  ionViewWillLeave(){
    this.searchObservationsEvaluations = '';
    this.inspectionInProgress.inspectionInform.observations = this.observations;
    this.selectedObservationsList = this.currentSelectedObservationList;
    if(this.levelSelected){
      this.levelSelected.evaluations.forEach(evaluation => {
        evaluation.showOptions = false;
      });
      this.showObservationWithError(this.inspectionInProgress.inspectionInform.observations);
    }
    this.globalProvider.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_CURRENT_KEY,this.inspectionInProgress);
  }

}


