import { RestProvider } from './../../providers/rest/rest';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, Platform, App } from 'ionic-angular';
import { WorkPage } from '../work/work';
import { GlobalProvider } from '../../providers/global/global';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var google;
@Component({
  selector: 'page-editwork',
  templateUrl: 'editwork.html',
})
export class EditworkPage {

  obra : any;
  obra_refresh : any;

  initBool : boolean;

  enterprises : any[] = [];
  departments : any[] = [];
  provinces : any[] = [];
  districts : any[] = [];

  enterprise : any = {};
  department : any = {};
  province : any = {};
  district : any = {};

  myForm: FormGroup;
  latitude : number;
  longitude : number;
  map: any;
  subsToValidateDatesStartDate: any;
  subsToValidateDatesEndDate: any;
  minDate: any;
  maxDate: any;
  validTime : any = { 
    validDates: true
  };
  userRoles: any[] = [];
  userRolesNames: any[] = [];
  userRol : any = {};

  userRolesAux : any[] = [];
  userRolAux : any = {}; 

  userInfo: any;
  dataDidLoad: boolean;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public alertCtrl: AlertController,
              public globalProv : GlobalProvider,
              public restProvider : RestProvider,
              public platform : Platform,
              public app : App,
              public fb: FormBuilder) {
    this.dataDidLoad = false;
    this.initBool = true
    this.obra = this.navParams.get("obra");
    this.init();
  }

  ionViewWillLeave(){
    if(this.dataDidLoad){
      this.subsToValidateDatesStartDate.unsubscribe();
      this.subsToValidateDatesEndDate.unsubscribe();
    }
  }

  createForm(){

    this.department = this.departments.find(dep=>{
      return dep.id == this.obra.department.id
    });

    this.userRoles = [];
    this.userRoles = this.userInfo.people_types;
    this.userRoles = this.userRoles.filter(rol => {
      return rol.isSupervisor == 1;
    });

    this.userRol = this.userRoles.find(rol => {
      return rol.id == this.obra.pivot.people_type_id
    });

    return this.fb.group({
      name: [this.obra.description, [Validators.required]],
      address: [this.obra.address, [Validators.required]],
      start_date: [this.obra.start_date, [Validators.required]],
      end_date: [this.obra.end_date, [Validators.required]],
      enterprise:[this.enterprise,[Validators.required]],
      selected:[this.userRol,[Validators.required]],
      district:[this.obra.district,[Validators.required]],
      province:[this.obra.province,[Validators.required]],
      department:[this.department,[Validators.required]],
      zip_code:[this.obra.zip_code,[Validators.required]]
    });
  }

  initiateUserInfo(){
    this.userInfo = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY);
  }

  async init(){
    let loading = this.globalProv.preloader();
    loading.present();

    let enterpriseIsLoad : boolean = true;

    await this.restProvider.getData(GlobalProvider.API_ENTERPRISES)
    .then((data : any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.ENTERPRISE_KEY,data.enterprises);
    })
    .catch(err => {
      enterpriseIsLoad = false;
      this.globalProv.messagesToast('No se pudo actualizar lista de empresas.', undefined, undefined, "toast-warning")
    });

    await this.restProvider.getData(GlobalProvider.API_PEOPLE + this.obra.pivot.people_id)
    .then((userinfo : any) => {
      this.globalProv.setItemLocalStorage(GlobalProvider.PEOPLE_KEY, userinfo.people);
    })
    .catch(err => {
      this.globalProv.messagesToast("No se pudo actualizar toda la información del usuario", undefined, undefined, 'toast-warning');
    });

    this.departments = this.globalProv.getItemLocalStorage(GlobalProvider.ALL_DEPARTMENTS);
    this.provinces.push(this.obra.province);
    this.districts.push(this.obra.district);
    if (!enterpriseIsLoad){
      this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error");
      loading.dismiss();
      return;
    }
    this.enterprises = this.getEnterprises(this.globalProv.getItemLocalStorage(GlobalProvider.ENTERPRISE_KEY));
    this.enterprise = this.getCurrentEnterprise(this.enterprises)[0];
    this.initiateUserInfo();
    this.myForm = this.createForm();
    if(this.userInfo.type === 'Interno'){
      this.myForm.disable();
    }
    if(this.userInfo.enterprise_id != 2){
      this.myForm.controls['enterprise'].setValue(this.userInfo.enterprise_id);
    }
    this.dataDidLoad = true;
    [this.minDate, this.maxDate] = this.globalProv.CurrentAndNextdate();
    [this.subsToValidateDatesStartDate, this.subsToValidateDatesEndDate] = this.globalProv.changeIniDateAndSetFinDate(this.myForm, ['start_date','end_date'], this.validTime, false);

    setTimeout(() => {
      this.loadMap();
      loading.dismiss();
    }, 0);
  }

  loadMap(){
    let latitude : number = parseFloat(this.obra.latitude);
    let longitude : number = parseFloat(this.obra.longitude);

    this.latitude = latitude;
    this.longitude = longitude;
    
    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map2');
  
    // create LatLng object
    let myLatLng = {lat: latitude, lng: longitude};
  
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 14
    });

    let marker = new google.maps.Marker({
      position: myLatLng,
      map: this.map,
    });

    let geocoder = new google.maps.Geocoder;
    let infowindow = new google.maps.InfoWindow;

    this.getDescription(geocoder, myLatLng, infowindow, marker);
    
  
    google.maps.event.addListener(this.map, 'click', (e) => {
      marker.setPosition(e.latLng)
      this.latitude = e.latLng.lat();
      this.longitude = e.latLng.lng();
      let latlong = {lat: this.latitude, lng: this.longitude}
      infowindow.close();
      this.getDescription(geocoder, latlong, infowindow, marker)
    });
  }

  getDescription(geocoder, latlong, infowindow, marker){
    geocoder.geocode({'location': latlong}, function(results, status) {
      if (status === 'OK' && results[5]) {
        infowindow.setContent(results[5].formatted_address);
        infowindow.open(this.map, marker);
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }

  getEnterprises(enterprises : any[]): any[]{
    return enterprises.filter((entrerprise : any) =>{
      return entrerprise.id != this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).enterprise_id
    })
  }

  getCurrentEnterprise(enterprises : any[]): any[]{
    return enterprises.filter((entrerprise : any) =>{
      return entrerprise.id == this.obra.enterprise_id
    })
  }

  saveNewWork(){
    let loading = this.globalProv.preloader()
    loading.present();

    let enterprise_id;

    if(this.userInfo.enterprise_id == 2){
      enterprise_id = this.myForm.value.enterprise.id;
    }else{
      enterprise_id = this.obra.enterprise_id;
    }

    let newObra = {
      enterprise_id : enterprise_id,
      description : this.myForm.value.name,
      address : this.myForm.value.address,
      district_id : this.myForm.value.district.id,
      province : this.myForm.value.province.id,
      department : this.myForm.value.department.id,
      ubigeo: this.myForm.value.department.code + this.myForm.value.province.code + this.myForm.value.district.code,
      latitude : this.latitude,
      longitude : this.longitude,
      zip_code : this.myForm.value.zip_code,
      start_date : this.myForm.value.start_date,
      end_date : this.myForm.value.end_date,
      status : this.obra.status,
      selected : [
        {
          peopleTypes : { 
            id : this.myForm.value.selected.id
          },
          peoples : {
            id : this.userInfo.id
          }
        }
      ]
    }
    
    this.restProvider.putData(GlobalProvider.API_CONSTRUCTIONS_CRUD + "/" + this.obra.id, newObra)
    .then((data:any) => {
      this.globalProv.messagesToast("Se editó la obra", undefined, undefined, "toast-success")
      loading.dismiss();
      this.navCtrl.pop({animate : true, direction: 'backward'});
    })
    .catch((err)=>{
      this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
      loading.dismiss();
    });
  }

  save(loading : any){
    this.restProvider.putData(GlobalProvider.API_CONSTRUCTIONS_CRUD + '/' + this.obra.id, this.obra_refresh)
    .then((data:any) => {
      this.obra = this.obra_refresh.construction;
      this.globalProv.messagesToast("Se editó la obra", undefined, undefined, "toast-success")
      loading.dismiss();
    })
    .catch((err)=>{
      this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
      loading.dismiss();
    });
  }

  removeWork(){
    let loading = this.globalProv.preloader()
    loading.present();

    let alert = this.alertCtrl.create();
    alert.setTitle('¿Seguro que desea eliminar la obra?');
    alert.addButton({
      text: 'Cancelar',
      handler: data => {
        loading.dismiss();
      }
    });
    alert.addButton({
      text: 'Eliminar',
      handler: data => {
          this.restProvider.deleteData(GlobalProvider.API_CONSTRUCTIONS_CRUD + "/" + this.obra.id)
          .then((data:any) => {
            this.globalProv.messagesToast("Se eliminó la obra", undefined, undefined, "toast-success")
            loading.dismiss();
            this.navCtrl.setRoot(WorkPage);
          })
          .catch((err)=>{
            this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error")
            loading.dismiss();
          });
        }
      });
      alert.present();
  }

  back(){
    this.navCtrl.setRoot(WorkPage);
  }

  onDepartChange(): void {
    this.initBool = false;
    let department = this.myForm.get('department').value;
    this.restProvider.getData(GlobalProvider.API_DEPARTAMENT + "/" + department.id).then((data : any) =>{
      this.provinces = data.department.provinces;
      this.myForm.value.province = '';
      this.myForm.value.district = '';
    }).catch(err=>{
      this.provinces = [];
      this.globalProv.messagesToast("No se pudo actualizar la lista de provincias", undefined, undefined, "toast-warning")
    })
    this.districts = [];
  }

  onProvinChange(): void {
    let province = this.myForm.get('province').value;
    this.myForm.value.district = '';
    this.districts = this.provinces.find(provin =>{
      return provin.id == province.id;
    }).districts;
  }
}
