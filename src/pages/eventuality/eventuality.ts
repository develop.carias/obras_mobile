import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, Platform, App, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { GlobalProvider } from '../../providers/global/global';
import { RestProvider } from '../../providers/rest/rest';
import { PreviewInspectionPage } from '../previewInspection/previewInspection';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Downloader, DownloadRequest } from '@ionic-native/downloader';
import { PermissionProvider } from '../../providers/permissions/permissions';

@Component({
  selector: 'page-eventuality',
  templateUrl: 'eventuality.html',
})
export class EventualityPage {
  @ViewChild(Nav) nav: Nav;

  inspectionsReports : any[] = [];
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public platform : Platform,
              public app : App,
              public globalProv : GlobalProvider,
              public restProvider : RestProvider,
              public modalCtrl : ModalController,
              public file : File,
              public androidPermissions: AndroidPermissions,
              public fileTransfer : FileTransfer,
              public permitProvider : PermissionProvider,
              public downloader: Downloader) {
    this.ChangeBackButton();
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      let nav = this.app.getActiveNavs()[0];
      nav.setRoot(HomePage,{},{animate: true, direction: 'backward'});
    });
  }

  ionViewDidEnter(){
    let loading = this.globalProv.preloader();
    loading.present();

    this.restProvider.getData(GlobalProvider.API_INSPECTIONS_PER_PEOPLE + this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
    .then((data : any) => {
      this.inspectionsReports = data.people.inspections;
      this.inspectionsReports = this.inspectionsReports.filter(inspectionReport => {
        return (inspectionReport.status === 'Terminado');
      });
      loading.dismiss();
    })
    .catch(err => {
      loading.dismiss()
      this.globalProv.messagesToast("Servicio temporalmente no disponible", undefined, undefined, "toast-error");
    })
  }

  goViewInspection(report){
    this.navCtrl.push(PreviewInspectionPage, {report: report});
  }

  async download(report) {

    let url = encodeURI(RestProvider.API_URL + GlobalProvider.API_DOWNLOAD_FILE + report.id);
    let downloadFilePath = encodeURI(this.file.externalRootDirectory + 'Supervisión de Obras/' + "Informe de planificación " + report.planification_id + ".pdf");
    let request: DownloadRequest = {
      uri: url,
      title: 'Descargando informe de inpección',
      description: '',
      mimeType: '',
      visibleInDownloadsUi: true,
      notificationVisibility: 1,
      destinationUri: downloadFilePath
    };

    let isWriteExternalStoragePermission : boolean = await this.permitProvider.requestPermissions(1, "Debe proveer permisos para poder descargar el informe");

    if(isWriteExternalStoragePermission){
      this.downloader.download(request)
      .then((location: string) => {
        this.globalProv.messagesToast("Se ha finalizado la descarga", undefined, undefined, 'toast-success');
      })
      .catch((error: any) => {
        console.log("Herror al descargar informe", error);
        this.globalProv.messagesToast("Ha ocurrido un error en la descarga", undefined, undefined, 'toast-error');
      });
    }

    /* this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
        if (result.hasPermission) {
          this.downloader.download(request)
          .then((location: string) => {
            this.globalProv.messagesToast("Se ha finalizado la descarga", undefined, undefined, 'toast-success');
          })
          .catch((error: any) => {
            this.globalProv.messagesToast("Ha ocurrido un error en la descarga", undefined, undefined, 'toast-error');
          });
        } else {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(result => {
            if (result.hasPermission) {
              this.downloader.download(request)
              .then((location: string) => {
                this.globalProv.messagesToast("Se ha iniciado la descarga", undefined, undefined, 'toast-success');
              })
              .catch((error: any) => {
                this.globalProv.messagesToast("Ha ocurrido un error en la descarga", undefined, undefined, 'toast-error');
              });
            }
          });
        }
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    ); */
  }

}
