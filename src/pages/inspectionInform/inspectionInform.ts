import { ListPage } from './../list/list';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Nav, Platform, App } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { GlobalProvider } from '../../providers/global/global';
import { PermissionProvider } from '../../providers/permissions/permissions';

@Component({
  selector: 'page-inspection-inform',
  templateUrl: 'inspectionInform.html'
})

export class InspectionInformPage {
  @ViewChild(Nav) nav: Nav;
  observationsForTheView : any [] = [];
  test : string;

  data: any=[];

  inspection: any = {}

  informPersist_array : any[] = [];

  user : any = {}

  names_present: string [] = []
  inspectionInform_present: any = {}

  observationTypes : any[] = [];
  peoplesFix: any[]=[];
  auxMediaArray: any[];
  
  constructor(public navCtrl: NavController, 
              public alertController: AlertController,
              public navParams: NavParams, 
              public globalProv : GlobalProvider,
              public platform : Platform,
              public app : App,
              public restProvider : RestProvider,
              public alertCtrl : AlertController,
              public permitProvider : PermissionProvider) {
    this.inspectionInform_present=navParams.get('aux_inform');
    this.inspection = navParams.get('inspection');
    this.ChangeBackButton();
    this.inspectionInform_present.observations = this.inspectionInform_present.observations.filter((item) => {
      if(!this.observationTypes.find(observationType => {return (observationType.id == item.observation_type_id)})){
        this.observationTypes.push(item.observation_type);
      }
      item.selected = item.evaluations.some(element => {
          return (element.selected == true);
        })
      item.evaluations = item.evaluations.filter((evaluation) => {
        evaluation.media = evaluation.media.filter(media => {
          return (media.image.urlMedia !== null || media.video.urlMedia !== null);
        });
        evaluation.hasMedia = evaluation.media.length > 0? true : false;
        return (evaluation.selected == true);  
      })
      item.evaluations.forEach(evaluation => {
        evaluation.options = evaluation.options.filter((option) => {
          return (option.id == evaluation.optionSelected_id);
        })
      });
      return (item.selected == true);
    });
    console.log("Observations after filtering", this.inspectionInform_present.observations);
    this.observationTypes.forEach(obsType => {
      let firstIteration : boolean = true;
      this.inspectionInform_present.observations.forEach(observation => {
        if(observation.observation_type_id == obsType.id){
          observation.IsFirst = firstIteration;
          if(firstIteration){
            firstIteration = false;
          }
          this.observationsForTheView.push(observation);
        }
      });
    });
  }

  ChangeBackButton(){
    this.platform.registerBackButtonAction(() => {
      this.goBack();
  });
  }

  constructInform(loading){

    let informPersist : any = {
      inspection : {
        id : this.inspection.inspection.id,
        total: 20
      },
      data: {
        name : this.user.name,
        email : this.user.email,
      },
      leader : this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id,
      selected : [
        {
          peopleTypes : {
            id : this.inspectionInform_present.leader.pivot.people_type_id
          },
          peoples : {
            id : this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id
          }
        }
      ]
    }
    
    if (this.inspectionInform_present.length != 0){
        informPersist.reports = this.informPersist_array
    } else {
        informPersist.inspection.total = 0
    }

    this.inspectionInform_present.peoples_present.forEach(element => {
      informPersist.selected.push({
        peopleTypes : {
          id : element.selected_peopleType.id
        },
        peoples : {
          id : element.id
        }
      })
    });

    this.restProvider.postData(GlobalProvider.API_SEND_REPORT, informPersist).then((data : any) =>{
      this.globalProv.messagesToast('Se ha enviado el informe', undefined, undefined, "toast-success");
      if(data.reports_id.length > 0){
        GlobalProvider.MEDIA_ARRAY = [];
        GlobalProvider.MEDIA_ARRAY = this.auxMediaArray;
        data.reports_id.forEach(object => {
          GlobalProvider.MEDIA_ARRAY.find(mediaToUpload => {
            return (mediaToUpload.evaluation_id === object.evaluation_id);
          }).report_id = object.report_id;
        });
        this.globalProv.setItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY,GlobalProvider.MEDIA_ARRAY);
        this.globalProv.uploadInQueueMediaArray(this.inspection.inspection.id).then(() => {
          console.log("Resolved promise of upload images");
          ListPage.updateListPage(this.navCtrl);
        }).catch(err => {
          console.log("Error of async method", err);
        });
      }else{
        localStorage.removeItem(GlobalProvider.CURRENT_INSPECTION_ID_KEY);
        localStorage.removeItem(GlobalProvider.MEDIA_ARRAY_KEY);
      }
      loading.dismiss().then(nav =>{
        this.navCtrl.setRoot(ListPage)
      });
    }).catch( err =>{
      console.log(err);
      loading.dismiss();
      this.globalProv.messagesToast('Servicio temporalmente no disponible', undefined, undefined, "toast-error");
    })
  }

  ionViewWillLeave(){
    localStorage.removeItem(GlobalProvider.PLANIFICATIONS_CURRENT_KEY);
  }

  async persistInform(){
    let loading = this.globalProv.preloader();
    loading.present();
    
    this.user.name = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).first_name + " " + 
                      this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).last_name;
    this.user.email = this.globalProv.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).email;

    if (this.inspection.inspection){
      this.createArray(this.inspectionInform_present.observations);
      console.log("Auxiliar MEDIA ARRAY", this.auxMediaArray);
       //////---------------------------
      this.constructInform(loading);
      //////---------------------------
    }else{

      let hasLocationPermission : boolean = await this.permitProvider.requestPermissions(0, "Se debe proveer permisos para poder continuar");

      let isLocationAccracy : boolean = await this.permitProvider.requestForAccuracy(false);

      if (!isLocationAccracy || !hasLocationPermission){
        loading.dismiss();
        return;
      }
  
      let positionPromise : any;

      if (isLocationAccracy){
        positionPromise = await this.globalProv.obtainCurrentPosition();
      }

      let isValidLocation : boolean = false;
      if (positionPromise.isPosition){
        let validate = {
          planification_id : this.inspectionInform_present.planification_id,
          latitude : positionPromise.latitude,
          longitude :  positionPromise.longitude
        }
        /**
         * TODO
         */
        await this.restProvider.postData(GlobalProvider.API_VALIDATION_LOCATION, validate).then((data : any) =>{
          isValidLocation = true;
          positionPromise.ableToValidate = true;
        }).catch(err=>{
          if(err.status === 401){
            positionPromise.ableToValidate = true;
            isValidLocation = false;
          }else{
            positionPromise.ableToValidate = false;
          }
        });
      }

      let requestInpection = {
        planification_id : this.inspectionInform_present.planification_id,
        latitude : positionPromise.latitude,
        longitude :  positionPromise.longitude,
        distance : null,
        time : null,
        total : 0,
        status : "En proceso"
      }

      if (isValidLocation){
        this.createInspection(requestInpection, loading);
      } else {
        const prompt = this.alertCtrl.create({
          title: positionPromise.ableToValidate? 'No se encuentra en la ubicación de la obra' : 'No se pudo validar ubicación',
          message: "¿Desea continuar de todas formas?",
          buttons: [
            {
              text: 'Cancelar',
              handler: data => {
                loading.dismiss();
              }
            },
            {
              text: 'Aceptar',
              handler: data => {
                this.createInspection(requestInpection, loading);
              }
            }
          ]
        });
        prompt.present();
      }
    }
  }

  createInspection(request:any, loading : any){
    this.restProvider.postData(GlobalProvider.API_INSPECTION_STORE, request).then((data : any) =>{
      this.globalProv.setItemLocalStorage(GlobalProvider.CURRENT_INSPECTION_ID_KEY, data.id);
      this.restProvider.getData(GlobalProvider.API_INSPECTION_CONSULT + data.id).then((data : any)=>{
        this.inspection = data;
        loading.dismiss();
        this.persistInform();
      });
    }).catch((err)=>{
      console.log("Error de crear inspección", err);
      loading.dismiss();
      this.globalProv.messagesToast("No se pudo realizar la petición, inténtelo de nuevo más tarde.");
    });
  }

  goNextStep(){
    this.persistInform();
  }

  createArray(obs : any[]){
    if (obs.length == 0){
      return
    }
    this.auxMediaArray = [];
    this.informPersist_array = [];
    obs.forEach(obs_grave => {
      
      obs_grave.evaluations.forEach(evaluation => {
        let currentInmform : any = {}
        currentInmform.inspection_id = this.inspection.inspection.id
        currentInmform.observation_id = obs_grave.id
        currentInmform.evaluation_id = evaluation.id
        currentInmform.evaluation_percentage = evaluation.percentage

        evaluation.options.forEach(option => {
          currentInmform.option_id = option.id
          currentInmform.evaluation_option_percentage = option.pivot.percentage
          currentInmform.comment = evaluation.comment
          currentInmform.urlVideo = evaluation.urlVideo
          currentInmform.urlImage = evaluation.urlImage
          currentInmform.latitude = evaluation.latitude
          currentInmform.longitude = evaluation.longitude
          currentInmform.time = evaluation.time
          if(currentInmform.urlImage | currentInmform.urlVideo){
            let mediaToUpload = {
              evaluation_id : evaluation.id,
              hasMedia : evaluation.hasMedia,
              media : evaluation.media
            };
            this.auxMediaArray.push(mediaToUpload);
          }
          this.informPersist_array.push(currentInmform)
        });
      });

    });
  }

  goBack(){
    this.navCtrl.pop({animate: true, direction: 'backward'});    
  }


}


