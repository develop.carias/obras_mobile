import { Injectable } from "@angular/core";
import { Platform } from "ionic-angular";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { GlobalProvider } from "../global/global";
import { LocationAccuracy } from "@ionic-native/location-accuracy";


@Injectable()
export class PermissionProvider {
    permissions : string[] = [
        this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
    ];
    platforms : string[] = [];
    isMobileWeb : boolean;
    isAndroid : boolean;
    isDesktop : boolean;

    constructor(public platform : Platform,
                public androidPermissions : AndroidPermissions,
                public locationAccuracy : LocationAccuracy,
                public globalProvider : GlobalProvider){
        this.platforms = this.platform.platforms();
        console.log("Plataforma actual", this.platforms);
        this.isMobileWeb = this.searchPlatform(this.platforms, 'mobileweb');
        this.isAndroid = this.searchPlatform(this.platforms, 'android');
        this.isDesktop = this.searchPlatform(this.platforms, 'core');
    }

    /**
     *
     * Método para pedir permisos
     *
     * @param numberOfPermission : 0 for location and 1 for writing to external storage
     *
     */
    async requestPermissions(numberOfPermission : number, message? : string) : Promise<boolean>{
        let hasPermission : boolean;
        let hasRequest : boolean = this.isMobileWeb || this.isDesktop;
        let permissionToRequest = this.permissions[numberOfPermission];

        if(!(this.isMobileWeb || this.isDesktop)){
            if(this.isAndroid){
                hasPermission = await this.isPermission(permissionToRequest);
                console.log("Has permission?", hasPermission);
                if(!hasPermission){
                    hasRequest = await this.makeRequest(permissionToRequest);
                    if(!hasRequest){
                        if(message){
                            this.globalProvider.messagesToast(message, undefined, undefined, 'toast-warning');
                        }
                    }
                }else{
                    hasRequest = hasPermission;
                }
            }
        }

        return hasRequest;
    }

    async isPermission(permissionToCheck : string) : Promise<boolean>{
        const checkPromise = await this.androidPermissions.checkPermission(permissionToCheck);
        return checkPromise.hasPermission;
    }

    async makeRequest(permissionToRequest : string) : Promise<boolean>{
        const checkPromise = await this.androidPermissions.requestPermission(permissionToRequest);
        return checkPromise.hasPermission;
    }

    searchPlatform(platforms : string[], platformToSearch : string) : boolean{
        return platforms.find(platform=>{return (platform==platformToSearch);})? true : false;
    }

    async requestForAccuracy(isNewWork : boolean) : Promise<boolean>{
        let isLocationAccracy : boolean = this.isMobileWeb || this.isDesktop;
        if(!(this.isMobileWeb || this.isDesktop)){

            await this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                if(canRequest) {
                  // the accuracy option will be ignored by iOS
                  isLocationAccracy = true;
                }else {
                  isLocationAccracy = false;
                }
            });
              
            await this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            .then(() => {
                isLocationAccracy = true;
            }, error => {
                console.log('Error requesting location permissions', error);
                if(error.code === this.locationAccuracy.ERROR_USER_DISAGREED){
                    isLocationAccracy = false;
                    if(!isNewWork){
                        this.globalProvider.messagesToast('Debe activar la ubicación para poder continuar', undefined, undefined, "toast-warning");
                    }
                }
            }).catch(err => {
                isLocationAccracy = false;
                console.log("Error del catch para el request de location accuracy", err);
            });
        }
        
        return isLocationAccracy;
    }

    isPlatform(platformToCheck : string) : boolean{
        return this.platform.is(platformToCheck);
    }
}