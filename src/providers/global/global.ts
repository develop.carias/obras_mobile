import { Injectable } from "@angular/core";
import { Platform, ToastController, LoadingController, AlertController, App } from "ionic-angular";
import { RestProvider } from "../rest/rest";
import { MyApp } from "../../app/app.component";
import { Keyboard } from '@ionic-native/keyboard';
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";
import { FormGroup } from "@angular/forms";
import { Observable } from "rxjs/Observable";
import { File } from "@ionic-native/file";
import { Subscription } from "rxjs/Subscription";
import { Geolocation } from '@ionic-native/geolocation';

declare var LZString
@Injectable()
export class GlobalProvider {

  //Variables globales para accesar a los metodos de Getdata
  static API_PEOPLE_TYPES : string = 'people-types';
  static API_ENTERPRISES : string = 'enterprise-app';
  static API_ENTERPRISE_DATA : string = 'enterprise/';
  static API_PEOPLES : string = 'people'
  static API_LOGIN : string = 'auth/login-app';
  static API_REGISTER : string = 'auth/register-app';
  static API_ALL_DATA : string = 'people/constructions-supervisor/';
  static API_USER : string = 'user/';
  static API_PEOPLE : string = 'people/';
  static API_PLANIFICATIONS_PER_PEOPLE : string = 'people/planifications/';
  static API_INSPECTIONS_PER_PEOPLE : string = 'people/inspections/';
  static API_CONSTRUCTIONS_PER_PEOPLE : string = 'people/constructions/';
  static API_CONSTRUCTIONS_CRUD : string = 'construction';
  static API_PLANIFICATION_CRUD : string = 'planification';
  static API_INSPECTION_STORE : string = 'inspection';
  static API_INSPECTION_CONSULT : string = 'inspection/';
  static API_SEND_REPORT : string = 'report';
  static API_EDIT_PROFILE : string = 'auth/edit-profile/';
  static API_PROTOCOLS : string = 'protocol';
  static API_DOWNLOAD_FILE : string = 'pdf/';
  static API_DEPARTAMENT : string = 'department';
  static API_VALIDATION_LOCATION : string = 'validate-location';
  static API_FINISH_PLANIFICATION : string = 'finish-planification/';

  static API_UPLOAD_PROFILE_PHOTO : string = 'auth/edit-profile-photo/';
  
  //Llaves para LocalStorage
  static HAS_USER_AGREED_KEY : string = 'has_user_agreed';
  static ALL_DATA_KEY : string = "all_data";
  static USER_KEY : string = "user";
  static PEOPLE_KEY : string = "people";
  static ENTERPRISE_KEY : string = "enterprise";
  static PEOPLE_TYPE_KEY : string = "peopleType";
  static PLANIFICATIONS_KEY : string = "planifications";
  static PLANIFICATIONS_CURRENT_KEY : string = "planifications_current";
  static CONSTRUCTIONS_KEY : string = "constructions";
  static ALL_PEOPLES : string = "all_peoples";
  static ALL_DEPARTMENTS : string = 'all_departments'
  static MEDIA_ARRAY_KEY : string = 'media_array';
  static REPORT_ID_ARRAY_KEY : string = 'report_id';
  static CURRENT_INSPECTION_ID_KEY : string = 'current_inspection_id';
  static FINISHED_CURRENT_PLAN_KEY: string = 'finished_current_plan_bool';

  //Variables globales para funcionalidad
  static keyboardWillShow : boolean = false;
  static APP_DIRECTORY_INTERN_MEMORY : string = 'Superv_de_Obras';
  static APP_DIRECTORY_PATH_INTERN_MEMORY : string;
  static MEDIA_ARRAY : any[] = [];
  static REPORT_ID_ARRAY : number[] = [];
  static finishedQueueUpload: boolean = true;
  static finishedCurrentPlanification: boolean = true;


  constructor(public plt: Platform, 
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController, 
              public alert: AlertController,
              public platform : Platform,
              public app : App,
              public restProvider : RestProvider,
              public keyboard : Keyboard,
              public fileTransfer : FileTransfer,
              private geolocation: Geolocation,
              public file : File) {
  }

  async promiseToTest(success : boolean) : Promise<any>{
    let promiseToReturn : Promise<any> = new Promise((resolve, reject) => {
      setTimeout(() => {
        if(success){
          resolve('Test promise resolved');
        }else{
          reject('Test promise rejected');
        }
      }, 5000);
    });
    return promiseToReturn;
  }

  async obtainCurrentPosition() : Promise<any>{
    let positionPromise : any = {};
    const options = {
      enableHighAccuracy: true,
      timeout: 30000,
      maximumAge: 0
    };
    await this.geolocation.getCurrentPosition(options).then(position => {
      positionPromise = position.coords;
      positionPromise.isPosition = true;
    }).catch(error =>{
      positionPromise.ableToValidate = false;
      positionPromise.isPosition = false;
      console.log("Error de getcurrentposition", error);
      if(error.message == "Timeout expired"){
        this.messagesToast('Se excedió el tiempo de espera, asegurese de que tiene la ubicación activada', undefined, undefined, "toast-warning")  
      }else{
        this.messagesToast('No se pudo obtener la ubicación', undefined, undefined, "toast-warning")
      }
    });
    return positionPromise;
  }

  setParametersForFileTransfer(parametersOfFileTransfer : any, dataToSet : any, image : boolean){
    parametersOfFileTransfer.filePath = dataToSet.mediaFilePath;
    parametersOfFileTransfer.options.fileName = dataToSet.mediaFileName;
    parametersOfFileTransfer.options.fileKey = image? 'urlImage' : 'urlVideo';
  }

  async uploadInQueueMediaArray(inspection_id : any){
    GlobalProvider.finishedQueueUpload = false;
    let successUpload : boolean;
    GlobalProvider.MEDIA_ARRAY = this.getItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY).filter(evaluationMedia=>{
      return (evaluationMedia.hasMedia)
    });
    GlobalProvider.MEDIA_ARRAY.forEach(evaluationMedia => {
      evaluationMedia.media = evaluationMedia.media.filter(media_ele => {
        return (media_ele.image.urlMedia !== null || media_ele.video.urlMedia !== null);
      });
    });

    const fileTransfer: FileTransferObject = await this.fileTransfer.create();
  
    let options: FileUploadOptions = {
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    for (let i = 0; i < GlobalProvider.MEDIA_ARRAY.length; i++){

      let boolToTest : boolean = false;
      for (let j = 0; j < GlobalProvider.MEDIA_ARRAY[i].media.length; j++){
        boolToTest = !boolToTest;

        let parametersForFileTransfer = {
          filePath : '',
          options: options
        };
        if(GlobalProvider.MEDIA_ARRAY[i].media[j].image.urlMedia){
         await this.uploadFile(i, j, parametersForFileTransfer, GlobalProvider.MEDIA_ARRAY[i], GlobalProvider.MEDIA_ARRAY[i].media[j].image, true, fileTransfer, boolToTest);
        }
        if(GlobalProvider.MEDIA_ARRAY[i].media[j].video.urlMedia){
         await this.uploadFile(i, j, parametersForFileTransfer, GlobalProvider.MEDIA_ARRAY[i], GlobalProvider.MEDIA_ARRAY[i].media[j].video, false, fileTransfer, boolToTest);
        }
        this.setItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY, GlobalProvider.MEDIA_ARRAY);

      }
      GlobalProvider.MEDIA_ARRAY[i].hasMedia = GlobalProvider.MEDIA_ARRAY[i].media.some(checkMedia => {
        return (checkMedia.image.urlMedia !== null || checkMedia.video.urlMedia !== null);
      });
      this.setItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY, GlobalProvider.MEDIA_ARRAY);

    }

    GlobalProvider.finishedQueueUpload = true;

    successUpload = GlobalProvider.MEDIA_ARRAY.every(evaluationMedia => {
      return (!evaluationMedia.hasMedia);
    });

    if(successUpload){
      GlobalProvider.finishedCurrentPlanification = false;
      this.setItemLocalStorage(GlobalProvider.FINISHED_CURRENT_PLAN_KEY, GlobalProvider.finishedCurrentPlanification);
      await this.finishPlanification(inspection_id);
      GlobalProvider.MEDIA_ARRAY = [];
    }else{
      this.messagesToast("No se enviaron todos los archivos multimedia");
    }
    this.setItemLocalStorage(GlobalProvider.MEDIA_ARRAY_KEY, GlobalProvider.MEDIA_ARRAY);
  }

  async uploadFile(i : number, j : number,parametersForFileTransfer : any, currentEvaluationWithMedia : any, currentMediaOfEvaluation : any, image : boolean, fileTransfer: FileTransferObject, boolToTest? : boolean){
    this.setParametersForFileTransfer(parametersForFileTransfer, currentMediaOfEvaluation, image);
    let successfullUpload : boolean;
    /* await this.promiseToTest(boolToTest)
    .then(valueResolved => {
      console.log("Resultado de promesa de subir archivo", valueResolved);
      currentMediaOfEvaluation.urlMedia = null;
      successfullUpload = true;
    })
    .catch(err => {
      console.log("Error al subir archivo", err);
      successfullUpload = false;
    }); */
    await fileTransfer.upload(parametersForFileTransfer.filePath, RestProvider.API_URL + GlobalProvider.API_SEND_REPORT + '/' + currentEvaluationWithMedia.report_id, parametersForFileTransfer.options)
    .then(uploadPromise => {
      if(image){
        GlobalProvider.MEDIA_ARRAY[i].media[j].image.urlMedia = null;
      }else{
        GlobalProvider.MEDIA_ARRAY[i].media[j].video.urlMedia = null;
      }
      successfullUpload = true;
    })
    .catch(err => {
      console.log("Error al subir archivo", err);
      successfullUpload = false;
    });
    if(successfullUpload){
      await this.file.removeFile(parametersForFileTransfer.filePath.substr(0, parametersForFileTransfer.filePath.lastIndexOf('/') + 1), parametersForFileTransfer.options.fileName);
    }
    return;
  }

  async finishPlanification(inspection_id : number, isFinishingInform? : boolean){
    let inspectionId : string = inspection_id.toString();
    let editInspection = {
      statusPlanification : 'Terminado',
      statusInspection : 'Terminado',
      hasPlanification : 0
    }
    await this.restProvider.putData(GlobalProvider.API_FINISH_PLANIFICATION + inspectionId, editInspection)
      .then(result => {
        console.log("Resultado de finish planification", result);
        GlobalProvider.finishedCurrentPlanification = true;
        if(isFinishingInform){
          this.messagesToast("Se ha terminado el informe", undefined, undefined, "toast-success")
        }else{
          this.messagesToast("Se enviaron todos los archivos multimedia", undefined, undefined, 'toast-success');
        }
        localStorage.removeItem(GlobalProvider.FINISHED_CURRENT_PLAN_KEY);
        localStorage.removeItem(GlobalProvider.MEDIA_ARRAY_KEY);
        localStorage.removeItem(GlobalProvider.CURRENT_INSPECTION_ID_KEY);
      })
      .catch(err => {
        console.log("Error de finish planification", err);
        this.messagesToast('No se pudo terminar el informe');
      });
  }

  createDirectory(directoryPath : string, directoryName : string){
    this.file.checkDir(directoryPath, directoryName).then(existDirectory => {
      if(!existDirectory){
        this.file.createDir(directoryPath, directoryName, false);
      }
    }).catch(err => {
      console.log("Error de chequear el directorio en global",err);
      if(err.code == 1){
        this.file.createDir(directoryPath, directoryName, false);
      }
    });
  }

  CurrentAndNextdate() : string[]{
    let d = new Date();
    let minDate = d.toISOString().substring(0, 10);
    d.setDate(d.getFullYear() + 2);
    let maxDate = d.toISOString().substring(0, 10);
    return [minDate,maxDate];
  }

  dateIsMinor(dateini : string, datefin : string, isHours? : boolean) : boolean{
    let bool = false;
    let d1 = isHours? dateini : new Date(dateini);
    let d2 = isHours? datefin : new Date(datefin);
    if(d1 <= d2){
      bool = true;
    }
    return bool
  }

  trackChangesInForm(formGroup : FormGroup, formcontrol? : string) : Observable<any>{
    let trackChange : Observable<any>;
    if(formcontrol){
      trackChange = formGroup.get(formcontrol).valueChanges;
    }else{
      trackChange = formGroup.valueChanges;
    }
    return trackChange;
  }

  changeIniDateAndSetFinDate(formGroup : FormGroup,formcontrols : string[], validTime : {validDates : boolean, validHour? : boolean}, Isplanifications : boolean) : Subscription[]{
    let subsArray : Subscription[] = [];
    let equalDates : boolean = false;
    subsArray[0] = this.trackChangesInForm(formGroup, formcontrols[0]).subscribe(result => {
      equalDates = formGroup.get(formcontrols[0]).value == formGroup.get(formcontrols[1]).value;
      if(Isplanifications){
        if(!equalDates){
          validTime.validHour = true;
        }else{
          validTime.validHour = this.dateIsMinor(formGroup.get(formcontrols[2]).value,formGroup.get(formcontrols[3]).value, true);
        }
      }
      if(formGroup.get(formcontrols[1]).value != ''){
        validTime.validDates = this.dateIsMinor(formGroup.get(formcontrols[0]).value,formGroup.get(formcontrols[1]).value);
        if(!validTime.validDates){
          formGroup.controls[formcontrols[1]].setValue(formGroup.get(formcontrols[0]).value);
        }
      }else{
        formGroup.controls[formcontrols[1]].setValue(formGroup.get(formcontrols[0]).value);
      }
    });

    subsArray[1] = this.trackChangesInForm(formGroup, formcontrols[1]).subscribe(result => {
      equalDates = formGroup.get(formcontrols[0]).value == formGroup.get(formcontrols[1]).value;
      if(Isplanifications){
        if(!equalDates){
          validTime.validHour = true;
        }else{
          validTime.validHour = this.dateIsMinor(formGroup.get(formcontrols[2]).value,formGroup.get(formcontrols[3]).value, true);
        }
      }
      if(formGroup.get(formcontrols[0]).value != ''){
        validTime.validDates = this.dateIsMinor(formGroup.get(formcontrols[0]).value,formGroup.get(formcontrols[1]).value);
      }
    });

    if(Isplanifications){
      subsArray[2] = this.trackChangesInForm(formGroup, formcontrols[2]).subscribe(result => {
        if(formGroup.get(formcontrols[3]).value != ''){
          if(equalDates){
            validTime.validHour = this.dateIsMinor(formGroup.get(formcontrols[2]).value,formGroup.get(formcontrols[3]).value, true);
            if(!validTime.validHour){
              formGroup.controls[formcontrols[3]].setValue(formGroup.get(formcontrols[2]).value);
            }
          }
        }else{
          formGroup.controls[formcontrols[3]].setValue(formGroup.get(formcontrols[2]).value);
        }
      });
  
      subsArray[3] = this.trackChangesInForm(formGroup, formcontrols[3]).subscribe(result => {
        if(formGroup.get(formcontrols[2]).value != ''){
          if(equalDates){
            validTime.validHour = this.dateIsMinor(formGroup.get(formcontrols[2]).value,formGroup.get(formcontrols[3]).value, true);
          }
        }
      });
    }

    return subsArray;
  }

  hideKeyboard(){
    this.keyboard.hide();
  }

  preloader(msg?){
    return this.loadingCtrl.create({
      content: msg != undefined ? msg : 'Procesando su solicitud...'
    });
   }

  messagesToast(message, time?, position? , color?){
    let toast = this.toastCtrl.create({
       message: message,
       duration: time == undefined ? 3000 : time, 
       position: position == undefined ? "top" : position,
       cssClass: color == undefined ? "toast-error" : color,
     });

     toast.present()
   }

   setLocalStorageData(data : any){
     // Determina si es el lider de la planificación
    let planifications : any[] = [];
    if (data.user.people.planifications.length !== 0){
      data.user.people.planifications.forEach(plan=>{
        if (plan.peoples.find(people=>{
          return people.pivot.leader === 1 && people.pivot.people_id === data.user.people.id;
        }) && plan.status != 'Terminado'){
          planifications.push(plan);
        }
      });
    }
    data.user.people.planifications = planifications;
    this.setItemLocalStorage(GlobalProvider.ALL_DATA_KEY,data);
    console.log("All data", this.getItemLocalStorage(GlobalProvider.ALL_DATA_KEY));
    this.setItemLocalStorage(GlobalProvider.ENTERPRISE_KEY,data.enterprises);
    this.setItemLocalStorage(GlobalProvider.PEOPLE_TYPE_KEY,data.peopleTypes);

    this.setItemLocalStorage(GlobalProvider.PLANIFICATIONS_KEY,planifications);
    this.setItemLocalStorage(GlobalProvider.CONSTRUCTIONS_KEY,data.user.people.constructions);
    this.setItemLocalStorage(GlobalProvider.ALL_PEOPLES,data.peoples);
    this.setItemLocalStorage(GlobalProvider.ALL_DEPARTMENTS,data.departments)
    delete data.user.people.planifications;
    delete data.user.people.constructions;
    this.setItemLocalStorage(GlobalProvider.PEOPLE_KEY,data.user.people);
    delete data.user.people;
    this.setItemLocalStorage(GlobalProvider.USER_KEY,data.user);
    this.updateUserInfo()
   }

   async updateUserInfoApi(){
     await this.restProvider.getData(GlobalProvider.API_PEOPLE + this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id)
     .then((userinfo : any) => {
      this.setItemLocalStorage(GlobalProvider.PEOPLE_KEY, userinfo.people);
     })
     .catch(err => {
      this.messagesToast("No se pudo actualizar toda la información del usuario", undefined, undefined, 'toast-warning');
     });
     return;
   }

   updateUserInfo(){
    let first_name = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).first_name;
    let last_name = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).last_name;
    MyApp._user.isSupervisor = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).people_types.some(peopleType => {
                                  return peopleType.isSupervisor == 1;
                                });
    MyApp._user.name =first_name + ' ' + last_name;
    MyApp._user.email = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).email;
    MyApp._user.type = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).type;
    let userPhoto = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).photo;
    let userId = this.getItemLocalStorage(GlobalProvider.PEOPLE_KEY).id;
    if(userPhoto==null){
      MyApp._user.imgLoaded = true;
      MyApp._user.urlPhoto = "assets/imgs/dummy_profile.jpg";
    }else{
      let d = new Date();
      MyApp._user.urlPhoto = RestProvider.BASE_URL_SERVER + '/storage/people/' + userId + "-photo/" + userPhoto + '?a=' + d.getTime();
    }
   }

    updateUserData(){
      let user_data : any = this.getItemLocalStorage(GlobalProvider.USER_KEY)
      return this.restProvider.getData(GlobalProvider.API_ALL_DATA + user_data.id, "", true);/* 
      .then((data:any) => {
        console.log("Data inicial", data);
        this.setLocalStorageData(data);
      })
      .catch((err)=>{
        console.log(err);
        this.messagesToast("No se pudo actulizar toda la información.",undefined,undefined,"toast-warning")
      }); */
    }

    setItemLocalStorage(item,value){
    localStorage.setItem(item, this.compressObject(value))
    }

    compressObject(value) {
        return LZString.compressToBase64(JSON.stringify(value))
    }

    getItemLocalStorage(item){ 
        return this.existsItemLocalStorage(item)===true?this.decompressObject(localStorage.getItem(item)):{}
    }

    existsItemLocalStorage(item){
        return localStorage.getItem(item)===null?false:true;
    }

    decompressObject(value) {
        return JSON.parse(LZString.decompressFromBase64(value))
    }

}